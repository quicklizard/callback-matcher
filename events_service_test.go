package callback_matcher

import (
	"testing"
)

func TestEventsService_HasMatchingHandlers(t *testing.T) {
	s := NewEventsService("http://callbacks-manager.staging.usea.quicklizard.service")
	s.Start()
	m1 := s.HasMatchingHandlers("echo", "demo", "default")
	m2 := s.HasMatchingHandlers("campaign_changed", "dummy", "default")
	m3 := s.HasMatchingHandlers("manual_override", "dummy", "default")
	if m1 != true {
		t.Errorf("expected demo client key to match echo event handler")
	}
	if m2 != true {
		t.Errorf("expected dummy client key to match campaign_changed event handler")
	}
	if m3 != false {
		t.Errorf("expected dummy client key not to match manual_override event handler")
	}
	s.Stop()
}

package callback_matcher

import (
	"encoding/json"
	"fmt"
	"github.com/go-resty/resty/v2"
	"log"
	"sync"
	"time"
)

type EventsService struct {
	client *resty.Client
	done   chan bool
	events Events
	lock   sync.Mutex
	ticker *time.Ticker
}

func NewEventsService(host string) *EventsService {
	client := resty.New().SetHostURL(host).
		SetTimeout(5 * time.Second).
		SetRetryCount(3).
		SetRetryWaitTime(300 * time.Millisecond)
	ticker := time.NewTicker(1 * time.Minute)
	return &EventsService{client: client, done: make(chan bool, 1), ticker: ticker}
}

func (s *EventsService) Start() {
	s.loadEvents()
	go s.sync()
}

func (s *EventsService) Stop() {
	s.done <- true
}

func (s *EventsService) HasMatchingHandlers(event, clientKey, channel string) bool {
	return s.events.matches(event, clientKey, channel)
}

func (s *EventsService) sync() {
	for {
		select {
		case <-s.ticker.C:
			if err := s.loadEvents(); err != nil {
				log.Printf("error syncing events - %v", err)
			}
		case <-s.done:
			return
		}
	}
}

func (s *EventsService) loadEvents() error {
	var events = make([]Event, 0)
	var page int = 1
	var err error
PageLoop:
	for {
		var res *resty.Response
		var result Events
		var params = map[string]string{
			"page":     fmt.Sprintf("%d", page),
			"per_page": "100",
		}
		res, err = s.client.R().SetQueryParams(params).Get("/v1/events")
		if err != nil {
			break PageLoop
		} else {
			err = json.Unmarshal(res.Body(), &result)
			if err != nil {
				break PageLoop
			} else if len(result) == 0 {
				break PageLoop
			} else {
				for _, e := range result {
					events = append(events, e)
				}
			}
		}
		page += 1
	}
	if err != nil {
		return err
	}
	s.lock.Lock()
	s.events = events
	s.lock.Unlock()
	return nil
}

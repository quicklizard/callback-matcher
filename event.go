package callback_matcher

type Event struct {
	Id       string    `json:"id"`
	Handlers []Handler `json:"handlers"`
	Name     string    `json:"name"`
}

type Events []Event

func (e Events) matches(event, clientKey, channel string) bool {
	for _, ev := range e {
		if ev.Name == event {
			for _, h := range ev.Handlers {
				if evaluateFilters(h.Filters, clientKey, channel) {
					return true
				}
			}
		}
	}
	return false
}

// evaluateFilters evaluates a set of filters against a given message.
// this function iterate each inner array of type Filter,
// checks if a filter match the given massage, then apply 'and' operator
// between the inner results.
// then an 'or' operator is applied between the outer array results.
func evaluateFilters(filters Filters, clientKey, channel string) bool {
	var value string
	filtersResult := false

	for _, filtersSubGroup := range filters {
		subGroupResult := true

		for _, filter := range filtersSubGroup {
			if filter.Field == "client_key" {
				value = clientKey
			} else if filter.Field == "channel" {
				value = channel
			}
			subGroupResult = subGroupResult && filter.matches(value)
		}

		filtersResult = filtersResult || subGroupResult
	}

	return filtersResult
}

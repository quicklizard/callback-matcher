package callback_matcher

import (
	"fmt"
	"reflect"
	"regexp"
)

type Filter struct {
	Field    string      `json:"field"`
	Operator Operator    `json:"operator"`
	Val      interface{} `json:"value"`
}

type Filters [][]Filter

// Matches will check if a filter matches a given field based on its operator and value/values
func (f Filter) matches(value string) bool {
	val := reflect.TypeOf(f.Val).Kind()
	switch val {
	case reflect.Slice:
		return f.compareValToSlice(value)
	case reflect.String:
		return f.compareValToString(value)
	}
	return false
}

func (f Filter) compareValToSlice(value string) bool {
	t := f.Val.([]interface{})
	compare := make([]string, len(t))
	for i, v := range t {
		compare[i] = fmt.Sprint(v)
	}
	contains := false
Loop:
	for _, v := range compare {
		if v == value {
			contains = true
			break Loop
		}
	}
	switch f.Operator {
	case In:
		return contains == true
	case Nin:
		return contains == false
	}
	return false
}

func (f Filter) compareValToString(value string) bool {
	compare := reflect.ValueOf(f.Val).String()
	switch f.Operator {
	case Eq:
		return value == compare
	case Neq:
		return value != compare
	case Match:
		match, _ := regexp.MatchString(compare, value)
		return match
	}
	return false
}

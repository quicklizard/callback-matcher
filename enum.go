package callback_matcher

type HandlerType string
type Operator string

const (
	Function HandlerType = "function"
	Queue                = "queue"
)

const (
	Eq    Operator = "eq"
	Neq            = "neq"
	In             = "in"
	Nin            = "nin"
	Match          = "match"
)
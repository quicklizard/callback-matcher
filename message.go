package callback_matcher

import (
	"encoding/json"
	"fmt"
)

type Message struct {
	Channel   string `json:"channel"`
	ClientKey string `json:"client_key"`
	Event     string `json:"event"`
	Payload   string `json:"payload"`
	Uuid      string `json:"uuid"`
}

func (m *Message) UnmarshalJSON(data []byte) error {
	r := struct {
		Channel   string `json:"channel"`
		ClientKey string `json:"client_key"`
		Event     string `json:"event"`
		Payload   string `json:"payload"`
		Uuid      string `json:"uuid"`
	}{}
	err := json.Unmarshal(data, &r)
	if err != nil {
		return err
	} else if r.Channel == "" {
		return fmt.Errorf("channel field is required")
	} else if r.ClientKey == "" {
		return fmt.Errorf("client_key field is required")
	} else if r.Event == "" {
		return fmt.Errorf("event field is required")
	} else if r.Uuid == "" {
		return fmt.Errorf("uuid field is required")
	}
	m.ClientKey = r.ClientKey
	m.Channel = r.Channel
	m.Event = r.Event
	m.Payload = r.Payload
	m.Uuid = r.Uuid
	return nil
}

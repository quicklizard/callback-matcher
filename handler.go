package callback_matcher

type Handler struct {
	Id      string      `json:"id"`
	Kind    HandlerType `json:"kind"`
	Filters Filters     `json:"filters"`
}
